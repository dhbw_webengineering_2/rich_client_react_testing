import axios from "axios";

export default class TodoHttpClient {
    constructor() {
        this._todoBasePath = 'http://localhost:8080/todo';
        this._headers = {
            'Accept': 'application/json'
        }
    }

    async getAllTodos() {
        return axios.get(
            this._todoBasePath,
            {
                headers: this._headers
            },
        )
            .then((response) => response.data);
    }

    async getTodoById(todoId) {
        return axios.get(
            `${this._todoBasePath}/${todoId}`,
            {
                headers: this._headers
            },
        )
            .then((response) => response.data);
    }

    async saveTodo(todo) {
        return axios.post(
            this._todoBasePath,
            todo,
            {
                headers: {...this._headers, 'Content-Type': 'application/json'},
            },
        )
            .then((response) => response.data);
    }
}