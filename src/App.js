import './App.css';
import { createContext } from 'react';
import TodoHttpClient from './clients/todo-http-client';
import ListView from './components/pages/list-view/ListView';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';
import DetailView from './components/pages/detail-view/DetailView';
import EditView from './components/pages/edit-view/EditView';

export const TodoHttpClientContext = createContext(TodoHttpClient)

function App() {
  return (
    <div className="App">
    </div>
  );
}

export default App;
