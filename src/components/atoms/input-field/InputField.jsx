import React from 'react';
import PropTypes from 'prop-types';
import './input-field.css';

export default function InputField({ id, text, onTextChange }) {
    return (
        <input
            id={id}
            type="text"
            className='input-field'
            value={text}
            onChange={(e) => onTextChange(e.target.value)}
        >
        </input>
    );
};

InputField.propTypes = {
    id: PropTypes.string.isRequired,
    text: PropTypes.string,
    onTextChange: PropTypes.func,
};

InputField.defaultProps = {
    text: '',
    onTextChange: (_) => {},
};
