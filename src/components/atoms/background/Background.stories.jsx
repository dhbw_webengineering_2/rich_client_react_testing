import React from 'react';

import Background from './Background';

export default {
    title: 'Components/Atoms/Background',
    component: Background,
};

const Template = () => <Background />;

export const BackgroundClassic = Template.bind({});