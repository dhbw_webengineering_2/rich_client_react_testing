import React from 'react';
import PropTypes from 'prop-types';
import './button.css';

export default function Button({ primary, label, onClick, className }) {
  const mode = primary ? 'button--primary' : 'button--secondary';
  return (
    <button
      type="button"
      className={['button', mode, className].join(' ')}
      onClick={() => onClick()}
    >
      {label}
    </button>
  );
};

Button.propTypes = {
  primary: PropTypes.bool,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  className: PropTypes.string,
};

Button.defaultProps = {
  primary: false,
  onClick: undefined,
  className: '',
};
