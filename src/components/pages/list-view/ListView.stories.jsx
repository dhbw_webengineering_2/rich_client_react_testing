import React from 'react';
import { TodoHttpClientContext } from '../../../App';
import Todo from '../../../model/todo';
import { reactRouterDecorator } from '../../../storybook/ReactRouterDecorator';
import Background from '../../atoms/background/Background';
import ListView from './ListView';

export default {
    title: 'Components/Pages/ListView',
    component: ListView,
    decorators: [reactRouterDecorator],
};

const todoHttpClientMock = {
    getAllTodos() {
        return Promise.resolve([
            new Todo(1, 'Take the trash out', false, 'Mom told me to pick the organic waste to our compost in the garden.'),
            new Todo(2, 'Feed the cat', false, 'Cat needs to be fed until monday, otherwise it will starve.'),
            new Todo(3, 'Eat grandpa', true, 'Grandpa needs to be eaten until wednesday, otherwise he doesn\'t taste good any more.'),
            new Todo(4, 'Clean the bathroom', false, 'There are plants already growing in the shower.'),
        ]);
    }
}

const Template = (args) =>
    <TodoHttpClientContext.Provider value={todoHttpClientMock}>
        <Background>
            <ListView {...args} />
        </Background>
    </TodoHttpClientContext.Provider>;

export const ListViewClassic = Template.bind({});
