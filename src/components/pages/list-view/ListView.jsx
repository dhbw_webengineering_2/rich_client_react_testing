import React, { useContext, useEffect, useState } from 'react';
import './list-view.css';
import { TodoHttpClientContext } from '../../../App';
import ListViewItem from '../../organisms/list-view-item/ListViewItem';
import Background from '../../atoms/background/Background';
import { useNavigate } from "react-router-dom";
import Button from '../../atoms/button/Button';

export default function ListView() {
    const todoHttpClient = useContext(TodoHttpClientContext);
    const navigate = useNavigate();

    const [todos, setTodos] = useState(null)

    useEffect(() => {
        todoHttpClient.getAllTodos().then((todos) => setTodos(todos));
    }, []);

    return (
        <Background>
            {
                todos ? <div className='list-view'>
                    {
                        todos.map((todo) => <ListViewItem key={todo.id} todo={todo} onShowDetail={() => navigate(`/detail/${todo.id}`)} />)
                    }
                    <Button label='+' primary={true} onClick={() => navigate(`/edit`)} />
                </div> : <div>Loading</div>
            }
        </Background>
    );
};
