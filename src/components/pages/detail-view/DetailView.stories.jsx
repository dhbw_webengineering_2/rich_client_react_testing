import React from 'react';
import { TodoHttpClientContext } from '../../../App';
import Todo from '../../../model/todo';
import { reactRouterDecorator } from '../../../storybook/ReactRouterDecorator';
import Background from '../../atoms/background/Background';
import DetailView from './DetailView';

export default {
    title: 'Components/Pages/DetailView',
    component: DetailView,
    decorators: [reactRouterDecorator],
};

const todoHttpClientMock = {
    getTodoById(_) {
        return Promise.resolve(new Todo(1, 'Take the trash out', false, 'Mom told me to pick the organic waste to our compost in the garden.'));
    },
    saveTodo(todo) {
        return Promise.resolve(todo);
    }
}

const Template = (args) =>
    <TodoHttpClientContext.Provider value={todoHttpClientMock}>
        <Background>
            <DetailView {...args} />
        </Background>
    </TodoHttpClientContext.Provider>;

export const DetailViewClassic = Template.bind({});
