import React, { useContext, useEffect, useState } from 'react';
import './detail-view.css';
import { TodoHttpClientContext } from '../../../App';
import Background from '../../atoms/background/Background';
import InputCheckboxGroup from '../../molecules/input-checkbox-group/InputCheckboxGroup';
import Button from '../../atoms/button/Button';
import { useParams, useNavigate } from "react-router-dom";

export default function DetailView() {
    const todoHttpClient = useContext(TodoHttpClientContext);
    const { todoId } = useParams();
    const navigate = useNavigate();

    const [todo, setTodo] = useState(null)

    useEffect(() => {
        todoHttpClient.getTodoById(todoId).then((todo) => setTodo(todo));
    }, []);

    const editTodo = () => navigate(`/edit/${todo.id}`);

    const saveTodo = (todo) => {
        todoHttpClient.saveTodo(todo).then((savedTodo) => {
            setTodo(savedTodo);
            navigate(`/list`);
        });
    }

    return (
        <Background>
            {
                todo ? <div className='detail-view'>
                    <h1 className='detail-view--title'>{todo.title}</h1>
                    <div>
                        <p className='detail-view--description'>{todo.description}</p>
                        <InputCheckboxGroup
                            className='detail-view--checkbox-group'
                            id='check'
                            label='Done'
                            checked={todo.done}
                            onCheckboxChange={(checked) => setTodo({ ...todo, done: checked })}
                        />
                        <div>
                            <Button className='detail-view--edit-button' label='Edit' onClick={editTodo} />
                            <Button label='Save' onClick={() => saveTodo(todo)} primary={true} />
                        </div>
                    </div>
                </div> : <div>Loading</div>
            }
        </Background>
    );
};
