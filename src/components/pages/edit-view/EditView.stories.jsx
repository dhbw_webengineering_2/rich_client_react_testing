import React from 'react';
import { TodoHttpClientContext } from '../../../App';
import Todo from '../../../model/todo';
import { reactRouterDecorator } from '../../../storybook/ReactRouterDecorator';
import Background from '../../atoms/background/Background';
import EditView from './EditView';

export default {
    title: 'Components/Pages/EditView',
    component: EditView,
    decorators: [reactRouterDecorator],
    argTypes: {
        backgroundColor: { control: 'color' },
    },
};

const todoHttpClientMock = {
    getTodoById(_) {
        return Promise.resolve(new Todo(1, 'Take the trash out', false, 'Mom told me to pick the organic waste to our compost in the garden.'));
    },
    saveTodo(todo) {
        return Promise.resolve(todo);
    }
}

const Template = (args) =>
    <TodoHttpClientContext.Provider value={todoHttpClientMock}>
        <Background>
            <EditView {...args} />
        </Background>
    </TodoHttpClientContext.Provider>;

export const EditViewClassic = Template.bind({});
