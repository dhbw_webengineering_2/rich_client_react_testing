import React, { useContext, useEffect, useState } from 'react';
import './edit-view.css';
import { TodoHttpClientContext } from '../../../App';
import Background from '../../atoms/background/Background';
import Button from '../../atoms/button/Button';
import { useParams, useNavigate } from "react-router-dom";
import InputFieldGroup from '../../molecules/input-field-group/InputFieldGroup';
import Todo from '../../../model/todo';

export default function EditView() {
    const todoHttpClient = useContext(TodoHttpClientContext);
    const { todoId } = useParams();
    const navigate = useNavigate();

    const [todo, setTodo] = useState(new Todo())

    useEffect(() => {
        if (todoId) {
            todoHttpClient.getTodoById(todoId).then((todo) => setTodo(todo));
        }
    }, []);

    const saveTodo = (todo) => {
        todoHttpClient.saveTodo(todo).then((savedTodo) => {
            setTodo(savedTodo);
            navigate(`/detail/${savedTodo.id}`);
        });
    }

    return (
        <Background>
            {
                todo ? <div className='edit-view'>
                    <div>
                        <InputFieldGroup
                            className='edit-view--title-input-group'
                            id='edit-title'
                            label='Title'
                            text={todo.title}
                            onTextChange={(text) => setTodo({ ...todo, title: text })}
                        />
                        <InputFieldGroup
                            className='edit-view--description-input-group'
                            id='edit-description'
                            label='Description'
                            text={todo.description}
                            onTextChange={(text) => setTodo({ ...todo, description: text })}
                        />
                        <div>
                            <Button label='Save' onClick={() => saveTodo(todo)} primary={true} />
                        </div>
                    </div>
                </div> : <div>Loading</div>
            }
        </Background>
    );
};
