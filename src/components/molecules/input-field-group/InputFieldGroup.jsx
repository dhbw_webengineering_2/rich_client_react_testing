import React from 'react';
import PropTypes from 'prop-types';
import './input-field-group.css';
import InputField from '../../atoms/input-field/InputField';

export default function InputFieldGroup({ id, label, text, onTextChange, className }) {
    return (
        <div className={['input-field-group', className].join(' ')}>
            <label className='input-field-group--label' htmlFor={id}>{label}</label>
            <InputField id={id} text={text} onTextChange={onTextChange} />
        </div>
    );
};

InputFieldGroup.propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    text: PropTypes.string,
    onTextChange: PropTypes.func,
    className: PropTypes.string,
};

InputFieldGroup.defaultProps = {
    text: '',
    onTextChange: (_) => {},
    className: '',
};
