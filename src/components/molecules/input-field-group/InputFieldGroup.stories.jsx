import React from 'react';
import Background from '../../atoms/background/Background';

import InputFieldGroup from './InputFieldGroup';

export default {
    title: 'Components/Molecules/InputFieldGroup',
    component: InputFieldGroup,
};

const Template = (args) => <Background><InputFieldGroup {...args} /></Background>;

export const InputFieldGroupClassic = Template.bind({});
InputFieldGroupClassic.args = {
    id: 'input1',
    label: 'Input 1',
    text: 'some default text',
    onTextChange: (text) => {
        console.log(text);
    },
};
