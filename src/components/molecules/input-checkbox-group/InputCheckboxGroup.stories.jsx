import React from 'react';
import Background from '../../atoms/background/Background';

import InputCheckboxGroup from './InputCheckboxGroup';

export default {
    title: 'Components/Molecules/InputCheckboxGroup',
    component: InputCheckboxGroup,
};

const Template = (args) => <Background><InputCheckboxGroup {...args} /></Background>;

export const InputCheckboxGroupUnchecked = Template.bind({});
InputCheckboxGroupUnchecked.args = {
    id: 'input1',
    label: 'Checkbox unchecked',
    disabled: false,
    checked: false,
    onCheckboxChange: (checked) => {
        console.log(checked);
    },
};

export const InputCheckboxGroupChecked = Template.bind({});
InputCheckboxGroupChecked.args = {
    id: 'input1',
    label: 'Checkbox checked',
    disabled: false,
    checked: true,
    onCheckboxChange: (checked) => {
        console.log(checked);
    },
};

export const InputCheckboxGroupDisabled = Template.bind({});
InputCheckboxGroupDisabled.args = {
    id: 'input1',
    label: 'Checkbox disabled',
    disabled: true,
    checked: false,
    onCheckboxChange: (checked) => {
        console.log(checked);
    },
};

