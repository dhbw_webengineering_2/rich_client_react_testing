import React from 'react';
import PropTypes from 'prop-types';
import './input-checkbox-group.css';

export default function InputCheckboxGroup({ id, label, disabled, checked, onCheckboxChange, className }) {
    return (
        <div className={['input-checkbox-group', className].join(' ')}>
            <label className='input-checkbox-group--label' htmlFor={id}>{label}</label>
            <input id={id} type='checkbox' disabled={disabled} checked={checked} onChange={(e) => onCheckboxChange(e.target.checked)} />
        </div>
    );
};

InputCheckboxGroup.propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    checked: PropTypes.bool,
    onTextChange: PropTypes.func,
    className: PropTypes.string,
};

InputCheckboxGroup.defaultProps = {
    disabled: false,
    checked: false,
    onCheckboxChange: (_) => { },
    className: '',
};
