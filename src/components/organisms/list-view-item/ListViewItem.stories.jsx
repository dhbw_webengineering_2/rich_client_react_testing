import React from 'react';
import Todo from '../../../model/todo';
import Background from '../../atoms/background/Background';

import ListViewItem from './ListViewItem';

export default {
    title: 'Components/Organisms/ListViewItem',
    component: ListViewItem,
};

const Template = (args) => <Background><ListViewItem {...args} /></Background>;

export const ListViewItemClassic = Template.bind({});
ListViewItemClassic.args = {
    todo: new Todo(1, 'Take the trash out', false, 'Mom told me to pick the organic waste to our compost in the garden.'),
    onShowDetail: (todoId) => {
        console.log(`Show details for todo: ${todoId}`);
    },
};
