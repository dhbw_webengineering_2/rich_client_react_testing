import React from 'react';
import PropTypes from 'prop-types';
import './list-view-item.css';
import InputCheckboxGroup from '../../molecules/input-checkbox-group/InputCheckboxGroup';
import Button from '../../atoms/button/Button';

export default function ListViewItem({ todo, onShowDetail }) {
    return (
        <div className='list-view-item'>
            <p className='list-view-item--title'>{todo.title}</p>
            <InputCheckboxGroup className='list-view-item--checkbox-group' id={`${todo.id}check`} disabled={true} label='Done' checked={todo.done} />
            <Button className='list-view-item--' label='Details' onClick={() => onShowDetail(todo.id)}/>
        </div>
    );
};

ListViewItem.propTypes = {
    todo: PropTypes.object.isRequired,
    onShowDetail: PropTypes.func,
};

ListViewItem.defaultProps = {
    onShowDetail: (_) => { },
};
