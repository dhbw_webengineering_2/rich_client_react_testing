export default class Todo {
    constructor(id, title, done, description) {
        this.id = id;
        this.title = title;
        this.done = done;
        this.description = description;
    }
}